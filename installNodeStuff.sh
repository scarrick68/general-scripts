#!/bin/bash

nvm install 6.0
npm install -g nodemon
npm install -g bower
npm install -g pm2
cd ~/
cd demo-site
npm install
cd public/javascripts
bower install
cd ../..
chmod +x setup-app.sh
./setup-app.sh
pm2 start ./bin/www --name 'demo site'
exit
