#!/bin/bash

# could take more input for flexibility, but hardcoding works for my purposes.

# start specified number of servers
echo -n 'Enter the number of instances you would like to start >'
read serverCount
ec2ids=($(aws ec2 run-instances --image-id ami-b1358ca6 --instance-type t2.micro --count $serverCount --key-name server\ 5-13-16 --security-group-ids sg-037a3b78 --subnet-id subnet-094b6121 --output text --query 'Instances[*].InstanceId'))

# write ec2 ids and elb ids to files
printf "%s\n" "${ec2ids[@]}" >> ec2Ids.txt
elbids=($(aws elb create-load-balancer --load-balancer-name demo-site --listeners Protocol=HTTP,LoadBalancerPort=80,InstanceProtocol=HTTP,InstancePort=80 --availability-zones us-east-1c us-east-1d --security-groups sg-d5dda6ae --output text --query 'DNSName'))
printf "%s\n" "${ec2ids[@]}" >> elbIds.txt
echo "all the ec2ids ${ec2ids[@]/#/}"

# create string of ec2 ids to pass to elb command
idList=${ec2ids[0]}
for (( i=1; i<${#ec2ids[@]}; i++ )); do 
	idList="$idList ${ec2ids[i]}"
done

# register servers with the load balancer
aws elb register-instances-with-load-balancer --load-balancer-name demo-site --instances $idList


# echo 'waiting one minute'
# sleep 60
# echo 'done waiting'

# get ips of ec2 instances for other script to start app server
ec2ips=($(aws ec2 describe-instances --filter "Name=instance-type,Values=t2.micro" --filter "Name=instance-state-code,Values=16" --output text --query Reservations[*].Instances[*].PublicIpAddress))

# code graveyard with potentially useful snippets for the future.

# aws ec2 describe-instances --filter Name=instance-type,Values=t2.medium --output json #--query  Instances[*].PublicIpAddress
# echo "all the ec2ips ${ec2ips[@]/#/}"

# for (( i=0; i<${#ec2ips[@]}; i++ )); do
# 	echo 'trying to start app servers'
# 	ssh -o StrictHostKeyChecking=no -i /Users/scarrick/Downloads/server5-13-16.pem ubuntu@${ec2ips[i]} 'bash ' << 'STARTAPP'
# 		cd ~/demo-site
# 		. ~/.bashrc
# 		. ~/.profile
# 	    pm2 start ./bin/www --name 'demo-site'
# 	    # exit
# 	STARTAPP
# 	echo 'start next app server'
# done