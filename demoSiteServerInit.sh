#!/bin/bash

cd ~/
sudo apt-get update

# install / config git
sudo apt-get install git -y
git config --global credential.helper cache
git config --global credential.helper 'cache --timeout=36000000'

# install nvm, node and global npm modules
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.2/install.sh | bash
. ~/.nvm/nvm.sh
nvm install 6.0
npm install -g nodemon
npm install -g bower
npm install -g pm2

# install mongodb
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
sudo apt-get install -y mongodb-org

# route port 80 to 3000 where application lives
sudo iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 3000

# clone in my project
git clone https://scarrick68@bitbucket.org/scarrick68/demo-site.git
cd ~/
git clone https://scarrick68@bitbucket.org/scarrick68/general-scripts.git
cd demo-site
npm install
cd public/javascripts
bower install
cd ../..
chmod +x setup-app.sh
./setup-app.sh
pm2 start ./bin/www --name 'demo site'
exit


# sftp private files that I don't put on github
sftp -i /Users/scarrick/Downloads/server5-13-16.pem ubuntu@$ip << 'UPLOAD_FILES'
	put ../demo-site/config/config.js ./demo-site/config/
	put ../demo-site/public/images/cover_photo.png ./demo-site/public/images/cover_photo.png
UPLOAD_FILES