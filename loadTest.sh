#!/bin/bash

elbUrl=$(aws elb describe-load-balancers --load-balancer-name demo-site --output text --query LoadBalancerDescriptions[*].DNSName)
servers=($(aws elb describe-load-balancers --load-balancer-name demo-site --output text --query LoadBalancerDescriptions[*].Instances[*]))
echo $elbUrl
echo 'enter concurrent users'
# read concurrent
echo 'enter total requests'
# read total
echo 'running load test'
concurrent=1000
total=100000
loadtest -n $total -c $concurrent -k http://$elbUrl > ./loadtest-results/${#servers[@]}-$concurrent-$total.txt
echo 'finished'
exit 0
