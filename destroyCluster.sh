#!/bin/bash

# Take down load balancer and ec2 instances created by launchCluster script

# Read in ec2 instance ids and make a string
idList=''
count=1
while IFS= read -r LINE; do
	if [ "$count" = "1" ]
	then
		echo $LINE
		idList=$LINE
		count=2
	else
		echo $LINE
		idList='$idList $LINE'
	fi
done < ec2Ids.txt

echo $idList